<?php
/**
 * Limesurvey plugin : Question attribute for subheading in list of choices and multiple choices
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017 Denis Chenu <https://www.sondages.pro>
 * @copyright 2017 DIALOGS <https://dialogs.ca/>
 * @license AGPL
 * @version 0.0.1
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class subHeadingInList extends \ls\pluginmanager\PluginBase
{
  static protected $name = 'subHeadingInList';
  static protected $description = 'Use strong for subheading in list of choice and multiple choice question.';

  /**
  * Add function 
  */
  public function init() {
    $this->subscribe('afterPluginLoad'); /* translation */
    
    $this->subscribe('newQuestionAttributes','addSubHeadingInList');
    $this->subscribe('beforeQuestionRender','setSubHeadingInList');
    //$this->subscribe('beforeSurveyPage','removeExcluded');
  }

  /**
   * Adding the constructed Question attribute settings
   */
  public function addSubHeadingInList()
  {
    $questionAttributes = array(
      'subHeadingInList'=>array(
        "types"=>"LM",
        'category'=>gT('Display'),
        'sortorder'=>150,
        'inputtype'=>'switch',
        "help"=>$this->_translate("Use strong tag on answer to set line as subheading."),
        "caption"=>$this->_translate('Use subheading'),
      ),
    );
    $this->getEvent()->append('questionAttributes', $questionAttributes);
  }

  /**
   * Set the javascript solution
   */
  public function setSubHeadingInList()
  {
    $oEvent = $this->event;
    if(in_array($oEvent->get('type'),["M","L"])) {
      $aAttributes=QuestionAttribute::model()->getQuestionAttributes($this->getEvent()->get('qid'));
      if(trim($aAttributes['subHeadingInList'])) {
        $qid=$oEvent->get('qid');
        $assetUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/subHeadingInList.js');
        App()->clientScript->registerScriptFile($assetUrl,CClientScript::POS_END);
        App()->clientScript->registerCssFile(Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/subHeadingInList.css'));
        App()->clientScript->registerScript("setSubHeadingInList{$qid}","subHeadingInList({$qid})",CClientScript::POS_END);
      }
    }
  }

  /** Translation part **/

  /**
   * Translate a string by this plugin
   * @param string
   * @return string
   */
  private function _translate($string){
      return Yii::t('',$string,array(),'messages'.get_class($this));
  }
  /**
   * Add this translation just after loaded all plugins
   * @see event afterPluginLoad
   */
  public function afterPluginLoad() {
      // messageSource for this plugin:
      $pluginMessageSource=array(
          'class' => 'CGettextMessageSource',
          'cacheID' => get_class($this).'Lang',
          'cachingDuration'=>3600,
          'forceTranslation' => true,
          'useMoFile' => true,
          'basePath' => __DIR__ . DIRECTORY_SEPARATOR.'locale',
          'catalog'=>'messages',// default from Yii
      );
      Yii::app()->setComponent('messages'.get_class($this),$pluginMessageSource);
  }
}
