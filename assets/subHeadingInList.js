/**
 * @file subHeadingInList javascript function
 * @author Denis Chenu
 * @copyright Denis Chenu <http://www.sondages.pro>
 * @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL v3.0
 */

function subHeadingInList(qid) {
  $("#question"+qid+" .answer-item strong").each(function(){
    $(this).closest(".answer-item").find("input").remove();
    $(this).closest(".answer-item").find("label:empty").remove();
    $(this).closest(".answer-item").addClass("subheading");
  });
}
